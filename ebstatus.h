#include <glib.h>

extern GString *status;
extern GMainLoop *main_loop;

typedef struct {
	int maximum;
	const char *symbol;
} Range;

extern gboolean use_pango_markup;

void status_separate ();
void status_unseparate ();
void status_append_ratio (int ratio, const Range *);
void status_start_color (int number, const Range *range);
void status_stop_color ();
void status_start_font (const char *family);
void status_stop_font ();

extern const Range range_block[];
extern const Range range_braille[];
extern const Range range_moon[];
extern const Range range_color_warning[];

void refresh (void);
