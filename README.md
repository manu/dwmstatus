ebstatus
========

This is my personal version of a status monitor for use with window managers
like [Sway](https://swaywm.org/), [i3](https://i3wm.org/) or
[dwm](https://dwm.suckless.org/). It is composed of several monitors for
different things as enumerated below, the actual feature set is configured at
compile time.

Compilation and configuration
-----------------------------

Compilation uses the [Meson](https://mesonbuild.com/) build system. The
compilation procdure goes like this:

    $ meson setup builddir
    $ cd builddir
    $ meson compile

For more details, refer to Meson's
[documentation](https://mesonbuild.com/Quick-guide.html#compiling-a-meson-project).

The list of active modules is specified by setting the `features` option in
`meson setup`, the value is a comma-separated list of module names. The
default value is `notmuch,networkmanager,volumes,battery,pulseaudio`, meaning
that all features are activated and shown in that particular order.
In any case, the current date and time are displayed last.

Modules
-------

### `battery`

Displays status information for each battery (charging, discharging, full)
with an estimation of remaining time in the current state.

### `networkmanager`

Displays information on network connections managed by [NetworkManager]:
wired, wifi (with signal strength), bluetooth, etc.

[NetworkManager]: https://wiki.gnome.org/Projects/NetworkManager/

### `notmuch`

Displays the number of unread mail messages as indexed by [Notmuch] and
optionally indicate when mail synchronization is ongoing. This uses the
configuration file of notmuch to locate the database. The following extra
settings can be used in this file:

- `ebstatus.query` is the query used for counting, the default value is
  `tag:unread`
- `ebstatus.symbol` is the symbol used in the status text, the default value
  is a symbol in the private area in [Font Awesome] version 4
- `ebstatus.sync_symbol` is the symbol used to represent synchronization, the
  default value is a symbol in the private area in [Font Awesome] version 4
- `sync.lock_file` is the path to a file whose existence signals that mail
  synchronization is in progress, as used by [Notmuch-tools].

[Notmuch]: https://notmuchmail.org/
[Font Awesome]: https://fontawesome.com/
[Notmuch-tools]: https://framagit.org/manu/notmuch-tools

### `pulseaudio`

Displays the output volume and status (speaker, headphone…) of sound output
according to [PulseAudio].

[PulseAudio]: https://www.freedesktop.org/wiki/Software/PulseAudio/.

### volumes

Displays a list of mounted removable volumes according to [GVfs].

[GVfs]: https://wiki.gnome.org/Projects/gvfs

Author
------

Emmanuel Beffara <manu@beffara.org>
