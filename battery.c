#include "ebstatus.h"

static const char *battery_path = "/sys/class/power_supply";

typedef struct {
	char *name;
	char *path;
	long charge_full;
} BatteryInfo;

static GPtrArray *battery_infos;

const Range range_battery[] = {
	{ 5, "" }, { 25, "" }, { 50, "" }, { 75, "" }, { 100, "" } };

/**
 * read_file:
 * @base: the first part of the path
 * @name: the second part of the path
 *
 * Read the contents of the file named @name in directory @base ad return it
 * as a string, with trailing whitespace removed.
 *
 * Returns: the text of the file  as a newly allocated string, or %NULL if the
 * file could not be read.
 */

static char *
read_file (const char *base, const char *name)
{
	static GString *buffer = NULL;
	char *contents, *end;

	if (buffer == NULL) buffer = g_string_new("");
	g_string_printf(buffer, "%s/%s", base, name);
	if (!g_file_get_contents(buffer->str, &contents, NULL, NULL))
		return NULL;
	for (end = contents; *end != 0; end++);
	while (end > contents && g_ascii_isspace(end[-1])) end--;
	*end = 0;
	return contents;
}

/**
 * read_file_as_number:
 * @base: the first part of the path
 * @name: the second part of the path
 * @value: a pointer to receive the value
 *
 * Read the contents of a file an integer and store in in @value.
 *
 * Returns: %TRUE on success, %FALSE if anything fails.
 */

static gboolean
read_file_as_number (const char *base, const char *name, long *value)
{
	char *contents = read_file(base, name);
	if (contents == NULL) return FALSE;
	*value = atol(contents);
	g_free(contents);
	return TRUE;
}

/**
 * init_one:
 * @name: the name of the power supply
 *
 * Initialize a structure describing a battery.
 *
 * Returns: a new structure, or %NULL in case any error occurs (including if
 * the given path does describe a battery).
 */

static BatteryInfo *
init_one (const char *name)
{
	char *text = NULL;
	BatteryInfo *info = g_new(BatteryInfo, 1);

	info->path = g_strdup_printf("%s/%s", battery_path, name);

	text = read_file(info->path, "type");
	if (text == NULL || strcmp(text, "Battery")) goto cancel;

	if (!read_file_as_number(info->path, "charge_full", &(info->charge_full)))
		goto cancel;

	info->name = g_strdup(name);
	return info;

cancel:
	g_free(text);
	g_free(info->path);
	g_free(info);
	return NULL;
}

/**
 * battery_init:
 *
 * Enumerate the batteries and their nominal charges.
 */

void
battery_init ()
{
	GDir *dir;
	GString *buffer;
	const char *name;
	BatteryInfo *info;

	dir = g_dir_open(battery_path, 0, NULL);
	if (dir == NULL) {
		g_printerr("Warning: cannot read %s.\n", battery_path);
		battery_infos = NULL;
		return;
	}

	battery_infos = g_ptr_array_new();
	buffer = g_string_new("");

	for (;;) {
		name = g_dir_read_name(dir);
		if (name == NULL) break;
		info = init_one(name);
		if (info != NULL) g_ptr_array_add(battery_infos, info);
	}

	g_dir_close(dir);
	g_string_free(buffer, TRUE);
}

/**
 * status_one:
 * @info: a #BatteryInfo structure
 *
 * Append the status of one battery to the status text.
 *
 * Returns: %TRUE if some battery status was obtained and printed, %FALSE otherwise
 */

static gboolean
status_one (BatteryInfo *info)
{
	long charge_now, current_now;
	char *text;
	gboolean result = TRUE;

	if (!read_file_as_number(info->path, "charge_now", &charge_now)) return FALSE;
	if (!read_file_as_number(info->path, "current_now", &current_now)) return FALSE;
	text = read_file(info->path, "status");

	/* g_string_append_printf(status, "%s ", info->name); */
	if (!strcmp(text, "Full")) {
		status_start_font("FontAwesome");
		g_string_append_printf(status, "");
		status_stop_font();
	} else if (!strcmp(text, "Discharging")) {
		int ratio = 100 * charge_now / info->charge_full;
		int time = 60 * charge_now / current_now;
		status_start_color(ratio, range_color_warning);
		status_start_font("FontAwesome");
		status_append_ratio(ratio, range_battery);
		status_stop_font();
		g_string_append_printf(status, "%d:%02d", time / 60, time % 60);
		status_stop_color();
	} else if (!strcmp(text, "Charging")) {
		int time = 60 * (info->charge_full - charge_now) / current_now;
		status_start_font("FontAwesome");
		g_string_append_printf(status, "");
		status_stop_font();
		if (time > 0)
			g_string_append_printf(status, "%d:%02d", time / 60, time % 60);
	} else {
		/* The only other possibility is "Not charging", when running
		 * on AC, with a full battery. In this case we stay silent. */
		result = FALSE;
	}

	g_free(text);
	return result;
}

/**
 * battery_status:
 *
 * Append the status of all batteries to the status text.
 *
 * Returns: %TRUE if some battery status was obtained and printed, %FALSE otherwise
 */

gboolean
battery_status ()
{
	guint i;
	gboolean printed = FALSE;

	if (battery_infos == NULL) return FALSE;

	for (i = 0; i < battery_infos->len; i++) {
		if (!status_one(battery_infos->pdata[i])) continue;
		g_string_append(status, ", ");
		printed = TRUE;
	}

	if (printed)
		g_string_truncate(status, status->len - 2);
	return printed;
}


