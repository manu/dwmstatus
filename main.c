#include <stdio.h>
#include <X11/Xlib.h>

#include "ebstatus.h"

#define DO(feature) \
	void feature##_init (); \
	gboolean feature##_status ();
FEATURES
#undef DO

Display *display;
GString *status;
GMainLoop *main_loop;
gboolean output_to_stdout = FALSE;
gboolean use_pango_markup = FALSE;

/* Formatting {{{1 */

/**
 * status_separate:
 *
 * Append a separator to the status text.
 */

void
status_separate ()
{
	g_string_append(status, "  ");
}

/**
 * status_unseparate:
 *
 * Remove a separator (as appended by status_separate()) from the status text.
 */

void
status_unseparate ()
{
	g_string_truncate(status, status->len - 2);
}

/**
 * status_append_ratio:
 * @ratio: a number between 0 and 100
 * @range: a table of #Range items for ratio symbols
 *
 * Append a character representing a ratio between 0 and 100 to the status
 * text, according to a table of ratio symbols.
 */

void
status_append_ratio (int ratio, const Range *range)
{
	ratio = CLAMP(ratio, 0, 100);
	while (range->maximum < ratio)
		range++;
	g_string_append(status, range->symbol);
}

const Range range_block[] = {
	{ 0, " " }, { 12, "▁" }, { 25, "▂" }, { 37, "▃" }, { 50, "▄" },
	{ 62, "▅" }, { 75, "▆" }, { 87, "▇" }, { 100, "█" } };

const Range range_moon[] = {
	{ 20, "🌕" }, { 40, "🌔" }, { 60, "🌓" }, { 80, "🌒" }, { 100, "🌑" } };

const Range range_braille[] = {
	{ 11, "⠀" }, { 22, "⡀" }, { 33, "⣀" }, { 44, "⣄" }, { 55, "⣤" },
	{ 66, "⣦" }, { 77, "⣶" }, { 88, "⣷" }, { 100, "⣿" } };

/**
 * status_start_color:
 * @value: a number between 0 and 100
 * @range: a table of #Range items for color names
 *
 * Append a color code to the status text, according to a table of color
 * names. The color will apply until the next call to status_stop_color().
 */

void
status_start_color (int number, const Range *range)
{
	if (!use_pango_markup) return;
	g_string_append(status, "<span color=\"");
	status_append_ratio(number, range);
	g_string_append(status, "\">");
}

const Range range_color_warning[] = {
	{ 5, "red" }, { 15, "orange" }, { 50, "yellow" }, { 100, "green" } };

/**
 * status_stop_color:
 *
 * Stop the effect of the last call to status_start_color().
 */

void
status_stop_color ()
{
	if (!use_pango_markup) return;
	g_string_append(status, "</span>");
}

/**
 * status_start_font:
 * @family: a font family name
 *
 * Append a code to activate a particular font family in the status text, if
 * available. The font will apply until the next call to status_stop_font().
 */

void
status_start_font (const char *family)
{
	if (!use_pango_markup) return;
	g_string_append_printf(status, "<span face=\"%s\">", family);
}

/**
 * status_stop_font:
 *
 * Stop the effect of the last call to status_start_font().
 */

void
status_stop_font ()
{
	if (!use_pango_markup) return;
	g_string_append(status, "</span>");
}

/* Date and time {{{1 */

/**
 * date_and_time:
 *
 * Append the date and time to the status text
 */

void
date_and_time ()
{
	GDateTime *now = g_date_time_new_now_local();
	g_string_append_printf(status, "%d/%d %d:%02d",
		g_date_time_get_day_of_month(now),
		g_date_time_get_month(now),
		g_date_time_get_hour(now),
		g_date_time_get_minute(now));
	g_date_time_unref(now);
}


/* Main {{{1 */

void
refresh (void)
{
	g_string_truncate(status, 0);

#define DO(feature) if (feature##_status()) status_separate();
	FEATURES
#undef DO

	date_and_time();
	g_print("%s", status->str);
}

static const GOptionEntry entries[] = {
	{ "pango", 'p', 0, G_OPTION_ARG_NONE, &use_pango_markup,
		"Enable Pango markup", NULL },
	{ "stdout", 's', 0, G_OPTION_ARG_NONE, &output_to_stdout,
		"Output status to stdout", NULL },
	{ NULL }
};

static void
print_status (const gchar *text)
{
	printf("%s\n", text);
	fflush(stdout);
}

static void
set_root_name (const gchar *text)
{
	XStoreName(display, DefaultRootWindow(display), text);
	XSync(display, False);
}

int
main (int argc, char **argv)
{
	GError *error = NULL;
	GOptionContext *context = g_option_context_new(NULL);
	g_option_context_add_main_entries(context, entries, NULL);
	if (!g_option_context_parse(context, &argc, &argv, &error)) {
		g_option_context_free(context);
		g_printerr("%s\n", error->message);
		return 1;
	}
	g_option_context_free(context);

	if (output_to_stdout) {
		g_set_print_handler(print_status);
	} else {
		display = XOpenDisplay(NULL);
		if (display == NULL) {
			g_printerr("Cannot open the display.\n");
			return 1;
		}
		g_set_print_handler(set_root_name);
	}

	status = g_string_new("");
	main_loop = g_main_loop_new(NULL, FALSE);

#define DO(feature) feature##_init();
	FEATURES
#undef DO

	refresh();

	g_timeout_add_seconds(5, (GSourceFunc)refresh, NULL);
	g_main_loop_run(main_loop);

	if (!output_to_stdout)
		XCloseDisplay(display);
	return 0;
}

