#include <NetworkManager.h>

#include "ebstatus.h"

NMClient *client;
GPtrArray *devices;

/**
 * device_state_char:
 * @state: a #NMDeviceState
 *
 * Provide a representation of a device's state as a single character.
 *
 * Returns: a character
 */

static char
device_state_char (NMDeviceState state)
{
	switch (state) {
	case NM_DEVICE_STATE_UNKNOWN:       return '?';
	case NM_DEVICE_STATE_UNMANAGED:     return ' ';
	case NM_DEVICE_STATE_UNAVAILABLE:   return '/';
	case NM_DEVICE_STATE_DISCONNECTED:  return '-';
	case NM_DEVICE_STATE_PREPARE:       return 'p';
	case NM_DEVICE_STATE_CONFIG:        return 'c';
	case NM_DEVICE_STATE_NEED_AUTH:     return 'a';
	case NM_DEVICE_STATE_IP_CONFIG:     return 'i';
	case NM_DEVICE_STATE_IP_CHECK:      return 'I';
	case NM_DEVICE_STATE_SECONDARIES:   return 's';
	case NM_DEVICE_STATE_ACTIVATED:     return '+';
	case NM_DEVICE_STATE_DEACTIVATING:  return '\\';
	case NM_DEVICE_STATE_FAILED:        return '!';
	}
	return '#';
}

/**
 * device_format_info:
 * @device: a #NMDevice
 * @out: a #GString buffer to write into
 *
 * Format the state of a device in a concise way and append the result to a
 * string buffer.
 *
 * Returns: %TRUE if some information was printed, %FALSE if the device was
 * skipped (e.g. because it is inactive).
 */

static gboolean
device_format_info (NMDevice *device, GString *out)
{
	NMDeviceState state;
	NMDeviceWifi *wifi;
	NMAccessPoint *ap;

	state = nm_device_get_state(device);
	switch (state) {
	case NM_DEVICE_STATE_DISCONNECTED:
	case NM_DEVICE_STATE_UNKNOWN:
	case NM_DEVICE_STATE_UNMANAGED:
	case NM_DEVICE_STATE_UNAVAILABLE:
		return FALSE;
	default:
		break;
	}

	if (NM_IS_DEVICE_ETHERNET(device)) {
		g_string_append(out, "");
		if (state != NM_DEVICE_STATE_ACTIVATED)
			g_string_append_c(out, device_state_char(state));
		return TRUE;

	} else if (NM_IS_DEVICE_BT(device)) {
		if (state == NM_DEVICE_STATE_DISCONNECTED)
			/* A known Bluetooth connection may appear
			 * disconnected if the underlying device is not
			 * paired. We just skip it, then. */
			return FALSE;
		g_string_append(out, "");
		if (state != NM_DEVICE_STATE_ACTIVATED)
			g_string_append_c(out, device_state_char(state));
		return TRUE;

	} else if (NM_IS_DEVICE_TUN(device)) {
		g_string_append(out, "");
		if (state != NM_DEVICE_STATE_ACTIVATED)
			g_string_append_c(out, device_state_char(state));
		return TRUE;

#ifdef NM_IS_DEVICE_LOOPBACK
	} else if (NM_IS_DEVICE_LOOPBACK(device)) {
		/* Just ignore the loopback device. */
		return FALSE;
#endif

	} else if (!NM_IS_DEVICE_WIFI(device)) {
		g_string_append_printf(out, "%s %c",
			nm_device_get_type_description(device),
			device_state_char(state));
		return TRUE;
	}

	wifi = NM_DEVICE_WIFI(device);
	ap = nm_device_wifi_get_active_access_point(wifi);
	if (ap == NULL) return TRUE;

	if (state == NM_DEVICE_STATE_ACTIVATED) {
		int strength = nm_access_point_get_strength(ap);
		status_start_color(strength, range_color_warning);
		if (use_pango_markup)
			g_string_append(out, "");
		else
			g_string_append_printf(status, "%d%%", strength);
		status_stop_color();
	} else {
		g_string_append(out, "");
		g_string_append_c(status, device_state_char(state));
	}

	return TRUE;
}

/**
 * networkmanager_status:
 *
 * Print the state of the list of devices in a concise way.
 *
 * Returns: %TRUE if some state was printed
 */

gboolean
networkmanager_status ()
{
	gboolean printed = FALSE;
	guint i;

	for (i = 0; i < devices->len; i++) {
		if (device_format_info(devices->pdata[i], status)) {
			status_separate();
			printed = TRUE;
		}
	}

	if (printed) status_unseparate();
	return printed;
}

/**
 * devices_add:
 * @client: a #NMClient
 * @device: a #NMDevice
 *
 * Register a new network device in the global table.
 */

static void
devices_add (NMClient *client, NMDevice *device)
{
	(void)client;
	g_ptr_array_add(devices, g_object_ref(device));
	g_signal_connect(device, "state-changed", G_CALLBACK(refresh), NULL);
}

/**
 * devices_remove:
 * @client: a #NMClient
 * @device: a #NMDevice
 *
 * Unregister a network device from the global table.
 */

static void
devices_remove (NMClient *client, NMDevice *device)
{
	(void)client;
	g_ptr_array_remove(devices, device);
}

/**
 * callback:
 *
 * The function that is called when the NetworkManager client object is ready.
 */

static void
callback (GObject *source, GAsyncResult *result, gpointer data)
{
	GError *error = NULL;
	(void)source; (void)data;

	const GPtrArray *all_devices;
	guint i;

	client = nm_client_new_finish(result, &error);
	if (client == NULL) {
		g_printerr("Error: %s\n", error->message);
		return;
	}

	all_devices = nm_client_get_devices(client);
	for (i = 0; i < all_devices->len; i++) {
		NMDevice *device = all_devices->pdata[i];
		if (nm_device_get_state(device) != NM_DEVICE_STATE_UNMANAGED)
			devices_add(client, device);
	}

	g_signal_connect(client, "device-added", G_CALLBACK(devices_add), NULL);
	g_signal_connect(client, "device-removed", G_CALLBACK(devices_remove), NULL);

	refresh();
}

/**
 * networkmanager_init:
 *
 * Initialise the NetworkManager status.
 */

void
networkmanager_init ()
{
	devices = g_ptr_array_new_with_free_func(g_object_unref);
	nm_client_new_async(NULL, callback, NULL);
}


